package main

import (
	"fmt"
	"math/rand"
	"time"
)

var playerAmount int
var dealerAmount int
var playerTotal int = 5

var playerBet int = 1

func main() {
	for playerBet > 0 {
		betting()
		fmt.Printf("\n")
		player(playerAmount)
		fmt.Printf("\n")
		dealer(dealerAmount)
		fmt.Printf("\n")
		winCalc()
	}
}

func random() int {
	rand.Seed(time.Now().UnixNano())
	min := 2
	max := 13
	return (rand.Intn(max-min) + min)
}

func player(int) {
	for {
		fmt.Println("Hit or stand?")
		var playerChoice string
		fmt.Scanln(&playerChoice)

		if playerChoice == "hit" {
			fmt.Println("Player Hits!")
			cardPick := random()
			playerAmount = playerAmount + cardPick
			fmt.Println(playerAmount)
		} else if playerChoice == "stand" {
			break
		}
		if playerAmount > 21 {
			fmt.Println("Player Busted!")
			break
		}
	}
}
func dealer(int) {
	for {
		if dealerAmount < 21 {
			if dealerAmount < 17 {
				cardPick := random()
				dealerAmount = dealerAmount + cardPick
				fmt.Println(dealerAmount)
			} else if dealerAmount == 21 {
				fmt.Println("Dealer got blackjack!")
			} else if dealerAmount >= 17 {
				fmt.Println("Dealer stands")
				break
			}
			if dealerAmount > 21 {
				fmt.Println("Dealer busted")
				break
			}

		}

	}
}

func betting() {
	fmt.Println("How much do you want to bet?")
	fmt.Scanln(&playerBet)
	if playerBet > playerTotal {
		fmt.Println(fmt.Sprint("You do not have enough money for that bet! "+"You only have ", playerTotal))
		betting()
	}
}

func winCalc() {
	if playerAmount > dealerAmount {
		if playerAmount > 21 {
			fmt.Println("Player Loses!")
			playerTotal = playerTotal - playerBet
		} else if dealerAmount > playerAmount {
			if dealerAmount > 21 {
				fmt.Println("Dealer Loses")
				playerTotal = playerBet*2 + playerTotal
			}
		} else if dealerAmount == 21 {
			fmt.Println("Dealer Wins!")
			playerTotal = playerTotal - playerBet
		} else {
			fmt.Println("The dealer wins!")
			playerTotal = playerTotal - playerBet
		}
	}
	main()
}
